import io
import os
import asyncio
from uuid import uuid4
from urllib.parse import unquote
from generate import generate
from sanic import Sanic
from sanic.exceptions import abort
from sanic.response import file, text

app = Sanic()

async def remove_file(file_name):
  await asyncio.sleep(2)
  os.remove(file_name)

@app.route('/')
async def index(request):
  return await file('index.txt')

@app.route('/api/v1.0/exeggutor/<height:int>')
async def exeggutor(request, height):
  if height > 30:
    return abort(400)
  
  file_name = '{}.png'.format(uuid4())

  image = await generate(height)
  image.save(file_name, 'PNG')
  loop = asyncio.get_event_loop()
  loop.create_task(remove_file(file_name))

  return await file(file_name)

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8080)

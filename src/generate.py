import io
import os
import praw
import asyncio
import aiohttp
from PIL import Image
from random import shuffle

reddit = praw.Reddit(
  client_id=os.environ['REDDIT_CLIENT_ID'],
  client_secret=os.environ['REDDIT_CLIENT_SECRET'],
  user_agent='Eggsecuter v1'
)

def fetch_random(num: int):
  posts = []
  for submission in reddit.subreddit('upvoteexeggutor').hot():
    if not submission.id == '5aj0ha':
      if any(s in submission.url for s in ('i.imgur.com', 'i.redd.it', 'i.reddituploads.com')):
        if not submission.over_18:
            posts.append(submission)
  shuffle(posts)
  return posts[:num]

def fetch_urls(num: int):
  posts = fetch_random(num)
  sticky = reddit.subreddit('upvoteexeggutor').sticky()
  last = reddit.submission(id='5aepwz')
  return [sticky] + posts + [last]

async def fetch_url(url):
  async with aiohttp.ClientSession() as session:
    async with session.get(url) as resp:
      if resp.status != 200:
          return None
      data = io.BytesIO(await resp.read())
      image = Image.open(data).convert('RGBA')
      return image

async def generate(height: int):
  posts = [x.thumbnail for x in fetch_urls(height)]
  images = [await fetch_url(x) for x in posts]
  
  widths, heights = zip(*(i.size for i in images))
  max_width = max(widths)
  total_height = sum(heights)

  image = Image.new('RGBA', (max_width, total_height))

  y_offset = 0
  for img in images:
    image.paste(img, (0, y_offset))
    y_offset += img.size[1]
  
  return image

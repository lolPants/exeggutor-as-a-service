FROM python:alpine3.6
WORKDIR /app

# Install Requirements
COPY requirements.txt .
RUN apk add --no-cache \
    build-base \
    zlib-dev \
    jpeg-dev && \
  pip install -r requirements.txt && \
  apk del build-base

COPY src/ .
EXPOSE 8080
CMD ["python", "-u", "__main__.py"]
